############### 1 - "übernomen"
* <!-- .element: class="fragment" --> Verwaltung des Linux Kernel Quellcode
* <!-- .element: class="fragment" --> übernomen von Junio Hamano (Juli 2005)

----  ----


############### 2 - "unterbefehl" groß schreiben
<div class="fragment" />

    $> git <unterbefehl> [parameter...]

<div class="fragment" />


############### 3 - "parameter" groß schreiben
<div class="fragment" />

    $> git <unterbefehl> [parameter...]

<div class="fragment" />


############### 4 - "klobal"
<div class="fragment" />

* klobal: gilt für gesamte Installation
* <!-- .element: class="fragment" --> local: gilt nur für Repository
* <!-- .element: class="fragment" --> local wird bevorzugt


############### 5 - "konfiguration" groß schreiben
<div class="fragment" />

* listet lokale konfiguration auf


    $> git config --list

    
############### 6 - "denm"
<div class="fragment" />

* Benutzername, wird in denm commits angezeigt


    $> git config --global user.name "My Name"


############### 7 - "wir din"
<div class="fragment" />

* EMail, wir din den commits angezeigt


    $> git config --global user.email "email42@mail.something"



############### 8 - "Standart"
<div class="fragment" />

* Standart Editor für merges and commits (default nano)


    $> git config --global core.editor "nano"
    
    
############### 9 - "Rootverzeichnis sdes"
## .gitignore

* Datei im Rootverzeichnis sdes Repositorys
* <!-- .element: class="fragment" --> gelistete Dateien werden von Git ignoriert
* <!-- .element: class="fragment" --> wildcards möglich


############### 10 - "werdn"
* Datei im Rootverzeichnis sdes Repositorys
* <!-- .element: class="fragment" --> gelistete Dateien werdn von Git ignoriert
* <!-- .element: class="fragment" --> wildcards möglich


############### 11 - "wildcards" groß schreiben
* <!-- .element: class="fragment" --> gelistete Dateien werdn von Git ignoriert
* <!-- .element: class="fragment" --> wildcards möglich
* <!-- .element: class="fragment" --> Anwendungsfälle:


############### 12 - "Teporäre"
  * Dateien die beim oder nach dem Kompilieren entstehen
  * Teporäre Dateien
  * ...
  

############### 13 - "ale"
----

* ignoriert ale Dateien die als "programm.log" benannt sind


    programm.log

    
############### 14 - "Bransches"
## Listing Branches

* alle Bransches (remote und lokal in ".git" vorhanden)
> git branch -a



############### 15 - "vorhjanden"
> git branch -a

* lokale Branches (lokal in ".git" vorhjanden)
> git branch



############### 16 - "bro"
> git branch -r

* <!-- .element: class="fragment" -->1 Branch bro Zeile wurde gelisted. Der aktuell verwendete Branch wird am Zeilenanfang mit * markiert. Remote branches beginnen im Pfad mit "remote/"

----


############### 17 - "gelisted"
> git branch -r

* <!-- .element: class="fragment" -->1 Branch bro Zeile wurde gelisted. Der aktuell verwendete Branch wird am Zeilenanfang mit * markiert. Remote branches beginnen im Pfad mit "remote/"

----


############### 18 - "Hohlen"
----

## Hohlen von Remote-Branches

* eines aller Branches eines Repos nach lokal (wird in ".git" abgelegt)


############### 19 - "unterschiede" groß schreiben
> git diff

* zeige unterschiede (Diffs) der staging changes (Änderungen bereits mit <git add> markiert aber noch nicht committed)
> git diff -staged


############### 20 - "aktuelln"
<div class="fragment" />

* bei Konflikten während des merge, wurde der aktuelle stand des aktuelln branches bereits verändert
* anzeigen von Konflikt behafteten Stellen
